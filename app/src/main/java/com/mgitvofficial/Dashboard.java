package com.mgitvofficial;

/**
 * Created by Eriga S Al Mansur
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.mgitvofficial.R;
import com.mgitvofficial.models.GYtSearch;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import static android.widget.Toast.makeText;

/**
 * Created by Eriga S Al Mansur 03/04/2019.
 */

public class Dashboard extends AppCompatActivity {

    BE.LoadingPrimary pd;
    Context c;
    String TAG;

    protected class getVideoStream extends AsyncTask<Void, Void, JSONObject> {
        private ProgressDialog pDialog;

        protected void onPreExecute() {
            super.onPreExecute();

//            DeveloperKey.VIDEO_ID = null;

            pDialog = new ProgressDialog(Dashboard.this);
            pDialog.setMessage("Mohon tunggu...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();




        }





        @Override
        protected JSONObject doInBackground(Void... params) {
            String str = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=" + DeveloperKey.CHANNEL_ID + "&eventType=live&type=video&key=" + DeveloperKey.DEVELOPER_KEY;
            URLConnection urlConn = null;
            BufferedReader bufferedReader = null;

            try {
                URL url = new URL(str);
                urlConn = url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                StringBuffer stringBuffer = new StringBuffer();
                String line;

                while ((line = bufferedReader.readLine()) != null) {
                    stringBuffer.append(line);
                }

                return new JSONObject(stringBuffer.toString());
            } catch(Exception ex) {
                return null;
            } finally {
                if(bufferedReader != null)
                {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            if (response != null) {
                try {
                    JSONArray ytItems = new JSONArray(response.getString("items"));
                    String ytVideoId = ytItems.getJSONObject(0).getJSONObject("id").getString("videoId");
                    DeveloperKey.VIDEO_ID = ytVideoId;
                    Intent intent = new Intent(getApplicationContext(), PlayerActivity.class);
                    startActivity(intent);
                } catch (Exception ex) {
                    makeText(Dashboard.this, "Sedang Tidak LIVE", Toast.LENGTH_SHORT).show();
                    Log.d("tidakLive",ex.getMessage());
                    Log.d("tidakLive2",ex.getLocalizedMessage());
                    Log.d("tidakLive3",ex.toString());
                }
            } else {
                makeText(Dashboard.this, "Gagal load video, cek internet.", Toast.LENGTH_SHORT).show();
            }

            pDialog.dismiss();
        }









    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);
        c = this;
        pd = new BE.LoadingPrimary(c);
        TAG = this.getClass().getSimpleName();
        // Swipe Refresh Layout


        // TENTANG
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.tentang);

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =  new Intent(getApplicationContext(), tentang.class);
                startActivity(i);
            }
        });

        // Video Kajian
        findViewById(R.id.linkvideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =  new Intent(getApplicationContext(), Playlist.class);
                startActivity(i);

            }


        });

        // Catatan Kajian
        findViewById(R.id.linkCatatan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =  new Intent(getApplicationContext(), BelumTersedia.class);
                startActivity(i);
            }
        });

        // Info Kajian
        findViewById(R.id.linkInfoKajian).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =  new Intent(getApplicationContext(), InfoKajian.class);
                startActivity(i);
            }
        });

        // MGI TV
        findViewById(R.id.linkMGI).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                DeveloperKey.CHANNEL_ID = "UCOM0_JMqjaPDeta_ObATZ0w";
//                new getVideoStream().execute();
//                Intent intent = new Intent(getApplicationContext(), PlayerActivity.class);
//                startActivity(intent);
                streaming();
            }
        });




    }

    private void streaming() {
        pd.show();
        AndroidNetworking.get("https://www.googleapis.com/youtube/v3/search?part=snippet&q=LIVE+STREAMING+MGI+TV&eventType=live&type=video&key=AIzaSyDzg_YDHTlo931VY3sk9t4KaXzdpu18jzs")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(GYtSearch.class, new ParsedRequestListener<GYtSearch>() {
                    @Override
                    public void onResponse(GYtSearch r) {
                        pd.dismiss();
                        if(r.getItems().size()>0) {
                            String videoId = r.getItems().get(0).getId().getVideoId();
//                            DeveloperKey.VIDEO_ID = ids;
                            Log.d("cekVideoId",videoId);
                            Bundle b = new Bundle();
                            Intent intent = new Intent(getApplicationContext(), PlayerActivity.class);
                            b.putString("ids",videoId);
                            intent.putExtras(b);
                            startActivity(intent);
                        }else {
                            BE.TShort("Tidak mendapatkan id streaming");
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        pd.dismiss();
                        BE.TShort(error.getErrorDetail());
                        Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                        Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                        Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                    }
                });
    }


}

