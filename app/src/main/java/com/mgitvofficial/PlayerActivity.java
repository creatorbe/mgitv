package com.mgitvofficial;

/**
 * Created by Eriga S Al Mansur
 */
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mgitvofficial.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;


public class PlayerActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    YouTubePlayer player;
    Button fullScreen;
    Intent i;
    Bundle b;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.yt_player);

        YouTubePlayerView playerView = (YouTubePlayerView)findViewById(R.id.youTubePlayerView);
        playerView.initialize(DeveloperKey.DEVELOPER_KEY,this);

        fullScreen = (Button)findViewById(R.id.btnFullScreen);
        fullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player.setFullscreen(true);
                player.play();
            }
        });

        i = getIntent();
        b = i.getExtras();
    }



    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean bol) {
        this.player = player;

        if (!bol) {
            player.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
//            player.loadVideo(DeveloperKey.VIDEO_ID);
            if(null != b.getString("ids")) {
                player.loadVideo(b.getString("ids"));
            }else{
                Toast.makeText(this, "Can't get streaming ID", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

}
