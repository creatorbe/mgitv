package com.mgitvofficial;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.mgitvofficial.R;

/**
 * Created by Eriga S Al Mansur 03/04/2019.
 */

public class SplashActivity extends AppCompatActivity {
    private ImageView logo_apps;
    private static int splashTimeOut=5000;

    @Override
   protected void onCreate (@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        logo_apps = (ImageView)findViewById(R.id.logo_apps);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(SplashActivity.this, Dashboard.class);
                startActivity(i);
                finish();
            }
        }, splashTimeOut);

        Animation myanim = AnimationUtils.loadAnimation(this, R.anim.animasi);
        logo_apps.startAnimation(myanim);


    }
}
