package com.mgitvofficial;

/**
 * Created by Eriga S Al Mansur
 */
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.mgitvofficial.R;

public class BelumTersedia extends AppCompatActivity {
    private WebView webViewSaya;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.belum_tersedia);

        // ambil id
        webViewSaya = (WebView) findViewById(R.id.tersedia);

        // aktifkan javascript
        webViewSaya.getSettings().setJavaScriptEnabled(true);

        // fungsi zoom (opsional)
        webViewSaya.getSettings().setBuiltInZoomControls(true);

        // isi url
        //webViewSaya.loadUrl("https://www.muhaaz.com");
        String nama_file = "layanan.html";
        webViewSaya.loadUrl("file:///android_asset/" + nama_file);
    }
}